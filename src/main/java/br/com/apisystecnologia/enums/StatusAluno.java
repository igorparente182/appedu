package br.com.apisystecnologia.enums;

public enum StatusAluno {
	
	MATRICULADO,
	TRANCADO,
	JUBILADO

}
