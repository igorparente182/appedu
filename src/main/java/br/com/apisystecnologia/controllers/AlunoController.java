package br.com.apisystecnologia.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.apisystecnologia.Services.AlunoService;
import br.com.apisystecnologia.Services.CursoService;
import br.com.apisystecnologia.entities.Aluno;

@Controller
public class AlunoController {

	@Autowired
	AlunoService alunoService;
	@Autowired
	CursoService cursoService;
	
	@GetMapping("/aluno")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("/Aluno/index");
		List<Aluno> alunos = alunoService.findAll();
		mv.addObject("alunos",alunoService.findAll());
		return mv;
	}
	
	@GetMapping("/aluno/add")
	public ModelAndView add(Aluno aluno) {
		ModelAndView mv = new ModelAndView("/Aluno/add");
		mv.addObject("aluno",aluno);
		return mv;
	}
	
	@GetMapping("/aluno/edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		return add(alunoService.findOne(id));
	}
	
	@GetMapping("/aluno/delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		alunoService.delete(id);
		return findAll();
	}
	
	@PostMapping("/save")
	public ModelAndView save(@Valid Aluno aluno,BindingResult result) {
		if(result.hasErrors()) {
            return add(aluno);
        }
         
        alunoService.save(aluno);
         
        return findAll();
	}
}
