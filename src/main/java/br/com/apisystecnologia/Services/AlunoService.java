package br.com.apisystecnologia.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.apisystecnologia.entities.Aluno;
import br.com.apisystecnologia.repositories.AlunoRepository;

@Service
public class AlunoService {
	
	@Autowired
	AlunoRepository repository;
	
	public List<Aluno> findAll(){
		return repository.findAll();
	}
	
	public Aluno findOne(Long id){
		return repository.findOne(id);
	}
	
	public Aluno save(Aluno aluno) {
		return repository.saveAndFlush(aluno);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}
	

}
