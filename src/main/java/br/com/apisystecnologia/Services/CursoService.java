package br.com.apisystecnologia.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.apisystecnologia.entities.Curso;
import br.com.apisystecnologia.repositories.CursoRespository;

@Service
public class CursoService {
	
	@Autowired
	CursoRespository repository;
	
	public List<Curso> findAll(){
		return repository.findAll();
	}
	
	public Curso findOne(Long id) {
		return repository.findOne(id);
	}
	
	public Curso save(Curso curso) {
		return repository.saveAndFlush(curso);
	}
	
	public void delete(Long id) {
		repository.delete(id);
	}

}
