package br.com.apisystecnologia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.apisystecnologia.Services.AlunoService;
import br.com.apisystecnologia.Services.CursoService;
import br.com.apisystecnologia.entities.Aluno;
import br.com.apisystecnologia.entities.Curso;
import br.com.apisystecnologia.enums.StatusAluno;

@SpringBootApplication
public class AppeduApplication implements ApplicationRunner{

	@Autowired
	AlunoService alunoService;
	@Autowired
	CursoService cursoService;
	public static void main(String[] args) {
		SpringApplication.run(AppeduApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		Curso curso = new Curso("321.32131.23","Sistema de informação","Estácio");
		//cursoService.save(curso);
		alunoService.save(new Aluno("Francisco Igor Parente de Freitas", curso, 21321312, 1, StatusAluno.MATRICULADO));
		
	}
}
