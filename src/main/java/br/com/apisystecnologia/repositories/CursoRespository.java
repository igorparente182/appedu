package br.com.apisystecnologia.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.apisystecnologia.entities.Curso;

@Repository
public interface CursoRespository extends JpaRepository<Curso, Long>{

	

}
